<?php namespace ProcessWire;

class RockReplacerConfig extends ModuleConfig {
	public function __construct() {
		$this->add(array(
			array(
				'name' => 'showvarstable',
				'type' => 'markup', 
				'label' => $this->_('Sample Replacements Table'),
				'description' => $this->_('You can render this table wherever you want using the replacementsTable() method of this module'), 
        'value' => modules('RockReplacer')->replacementsTable(),
			)
		));
	}
}
