<?php namespace RockReplacer;
class sample_pagepath extends Replacer {

  /**
   * description rendered in the replacements table
   */
  public function desc() {
    return $this->_('Sample replacement for Tags with no Content that replaces the tag with the Pages path. See the /replacements folder of this Module!');
  }

  // this will replace all occurrences of [sample_pagepath] with the return value
  public function replace($vars) {
    /**
     * $vars holds all variables passed as an array
     *
     * example:
     * modules('RockReplacer')->replace('the current page path is: [sample_pagepath]', ['page' => $page]);
     *
     * example output:
     * the current page path is: /demo-page
     */
    return $vars['page']->path;
  }

  // if you don't specify a search method it will take the default parent
  // that means it wraps the classname around [ ] and replaces this tag
  // in this case this would be [sample_pagepath]

}
