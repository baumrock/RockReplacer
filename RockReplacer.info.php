<?php namespace ProcessWire;
/**
 * Module info file that tells ProcessWire about this module. 
 */
$info = array(
	'title' => 'Tag-Replacement Module for ProcessWire',
	'version' => 2,
	'summary' => 'Similar to HannaCode this can replace Tags and simple IF-Statements',
	'singular' => true,
	'autoload' => false,
	'icon' => 'file-exchange',
);