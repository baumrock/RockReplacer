<?php namespace RockReplacer;
abstract class Replacer extends \ProcessWire\Wire {
  protected $RockReplacer;

  function __construct($RockReplacer) {
    $this->RockReplacer = $RockReplacer;
  }

  abstract public function desc();
  abstract public function replace($vars);

  /**
   * by default the search-string is the classname of the replacement wrapped in [ ]
   */
  public function search() {
    return '[' . $this->className() . ']';
  }
}
