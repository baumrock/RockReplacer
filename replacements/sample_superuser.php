<?php namespace RockReplacer;
class sample_superuser extends Replacer {

  /**
   * description rendered in the replacements table
   */
  public function desc() {
    return $this->_('Sample replacement for Tags with Content that shows the Content only to Superusers. See the /replacements folder of this Module!');
  }

  public function replace($vars) {
    /**
     * $vars holds all variables passed as an array
     *
     * example:
     * modules('RockReplacer')->replace('Hello there![su] You are the best![/su]');
     *
     * example output:
     * superuser: Hello there! You are the best!
     * user: Hello there!
     */
    if(!$this->user->isSuperuser()) return '';
    return $vars['val'];
  }

  // the search method has to have the {val} keyword
  // here it will not take the classname "sample_superuser" for the tags but the manually specified ones
  public function search() {
    return '[su]{val}[/su]';
  }

}
