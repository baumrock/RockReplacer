<?php namespace ProcessWire;

/**
 * mPDF Module for ProcessWire
 * @author Bernhard Baumrock, baumrock.com
 * Licensed under MIT
 */

class RockReplacer extends WireData implements Module {

  public $replacements;

	/**
	 * Initialize the module (optional)
	 */
	public function init() {
    // load the mpdf library
		$this->tempDir = $this->files->tempDir('RockMpdf');

		// load replacements

			// load the replacement class
			require_once(__DIR__ . '/Replacer.php');
		
			// find files in the folder replace and replace the tags by the files content
			$rii = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(__DIR__ . '/replacements/'));
			foreach ($rii as $fileInfo) {
				if(!$fileInfo->isFile()) continue;

				require_once($fileInfo->getPathname());
				$classname = '\RockReplacer\\' . $fileInfo->getBasename('.php');
				$this->replacements[] = new $classname($this);
			}
	}

	/**
	 * return html with replaced variables
	 */
	public function replace($html, $vars = []) {
		// replace all found tags
		foreach($this->replacements as $item) {
			// check if we have an if statement
			// it is an if-statement if there is a {val} tag
			if(strpos($item->search(), '{val}') !== false) {
				// this is an if-block, so we need a preg_replace
				$blocks = explode('{val}', $item->search());

				// fast strpos exit if start or end-tag is not found
				if(strpos($html, $blocks[0]) === false) continue;
				if(strpos($html, $blocks[1]) === false) continue;

				// we found both the start and the end tag
				// now lets find the {val} part of the replacement
				$re = '#' . preg_quote($blocks[0]) . '(.*?)' . preg_quote($blocks[1]) . '#s';
				preg_match_all($re, $html, $matches, PREG_SET_ORDER);

				foreach($matches as $match) {
					$vars = array_merge($vars, ['val' => $match[1]]);
					$html = str_replace($match[0], $item->replace($vars), $html);	
				}
			}
			else {
				// this is a simple tag-replacement
				// early exit if replace tag is not found to avoid calling the replace method
				if(strpos($html, $item->search()) === false) continue;
				$html = str_replace($item->search(), $item->replace($vars), $html);
			}
		}

		return $html;
	}

	/**
	 * return all replacements
	 */
	public function replacementsTable() {
		if(!is_array($this->replacements)) return;
		
		$table = $this->modules->get('MarkupAdminDataTable');
		$table->setID('variablestable');
		$table->sortable = true;
		
		// table header
		$table->headerRow([
			__('Tag'),
			__('Description'),
		]);

		// rows
		foreach($this->replacements as $item) {
			$cols = [];

			$cols[] = $item->search();
			$cols[] = $item->desc();

			$table->row($cols);
		}
		
		// render table
		return $table->render();
	}

}